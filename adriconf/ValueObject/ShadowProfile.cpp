#include "ShadowProfile.h"

ShadowProfile::ShadowProfile(const Profile_ptr &originalData) : originalData(originalData) {
    this->setName(originalData->getName());
    this->setPrimeDriverName(originalData->getPrimeDriverName());
    this->setIsUsingPrime(originalData->getIsUsingPrime());
    this->setExecutable(originalData->getExecutable());
    this->setDevicePCIId(originalData->getDevicePCIId());

    for(const auto& originalOption : originalData->getOptions()) {
        ProfileOption_ptr copiedOption = std::make_shared<ProfileOption>();
        copiedOption->setName(originalOption->getName());
        copiedOption->setValue(originalOption->getValue());

        this->addOption(copiedOption);
    }
}

void ShadowProfile::applyChangesToOriginalData() {
    this->originalData->setOptions(this->getOptions());
    this->originalData->setIsUsingPrime(this->getIsUsingPrime());
    this->originalData->setDevicePCIId(this->getDevicePCIId());
    this->originalData->setPrimeDriverName(this->getPrimeDriverName());
}
