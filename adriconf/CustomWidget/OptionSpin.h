#ifndef ADRICONF_OPTIONSPIN_H
#define ADRICONF_OPTIONSPIN_H

#include "gtkmm.h"
#include "../ValueObject/ProfileOption.h"

class OptionSpin : public Gtk::SpinButton {
private:
    ProfileOption_ptr optionPtr;

public:
    OptionSpin(const ProfileOption_ptr &optionPtr, int validStartValue, int validEndValue);

    void onOptionChanged();
};


#endif //ADRICONF_OPTIONSPIN_H
