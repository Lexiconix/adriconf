#include "OptionSwitch.h"

OptionSwitch::OptionSwitch(const ProfileOption_ptr &optionPtr) : optionPtr(optionPtr) {
    this->set_visible(true);
    if (optionPtr->getValue() == "true" || optionPtr->getValue() == "1") {
        this->set_active(true);
    }

    this->property_active().signal_changed().connect(sigc::mem_fun(this, &OptionSwitch::onOptionChanged));
}

void OptionSwitch::onOptionChanged() {
    if (this->optionPtr->getValue() == "true") {
        this->optionPtr->setValue("false");
        return;
    }

    if (this->optionPtr->getValue() == "false") {
        this->optionPtr->setValue("true");
        return;
    }

    if (this->optionPtr->getValue() == "1") {
        this->optionPtr->setValue("0");
        return;
    }

    this->optionPtr->setValue("1");
}
