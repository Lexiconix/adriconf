#include "GUI.h"

#include <boost/locale.hpp>
#include "Utils/ConfigurationResolver.h"
#include "Utils/Writer.h"
#include "Utils/ConfigurationLoaderInterface.h"
#include <fstream>
#include <exception>
#include <utility>
#include "version.h"
#include "CustomWidget/OptionSwitch.h"
#include "CustomWidget/OptionCombo.h"
#include "CustomWidget/OptionSpin.h"

GUI::GUI(
        LoggerInterface *logger,
        TranslatorInterface *translator,
        ConfigurationLoaderInterface *configurationLoader,
        ConfigurationResolverInterface *resolver,
        WriterInterface *writer
) : logger(logger),
    translator(translator),
    configurationLoader(configurationLoader),
    resolver(resolver),
    writer(writer),
    currentApp(nullptr),
    currentDriver(nullptr) {
    this->setupLocale();

    /* Load the configurations */
    this->driverConfiguration = this->configurationLoader->loadDriverSpecificConfiguration(this->locale);
    for (auto &driver : this->driverConfiguration) {
        driver.sortSectionOptions();
    }

    if (this->driverConfiguration.empty()) {
        throw std::runtime_error("No driver configuration could be loaded");
    }

    this->systemWideConfiguration = this->configurationLoader->loadSystemWideConfiguration();
    this->userDefinedConfiguration = this->configurationLoader->loadUserDefinedConfiguration();
    this->availableGPUs = this->configurationLoader->loadAvailableGPUs(this->locale);

    /* For each app setup their prime driver name */
    this->resolver->updatePrimeApplications(
            this->userDefinedConfiguration,
            this->availableGPUs
    );

    /* Merge all the options in a complete structure */
    this->resolver->mergeOptionsForDisplay(
            this->systemWideConfiguration,
            this->driverConfiguration,
            this->userDefinedConfiguration,
            this->availableGPUs
    );

    /* Filter invalid options */
    this->resolver->filterDriverUnsupportedOptions(
            this->driverConfiguration,
            this->userDefinedConfiguration,
            this->availableGPUs
    );

    this->logger->debug(this->translator->trns("Start building GTK gui"));

    /* Load the GUI file */
    this->gladeBuilder = Gtk::Builder::create();
    this->gladeBuilder->add_from_resource("/jlHertel/adriconf/DriConf.glade");

    /* Extract the main object */
    this->gladeBuilder->get_widget("mainwindow", this->pWindow);
    if (!pWindow) {
        this->logger->error(this->translator->trns("Main window object is not in glade file!"));
        return;
    }

    auto cssProvider = Gtk::CssProvider::create();
    cssProvider->load_from_resource("/jlHertel/adriconf/DarkTheme.css");

    this->pWindow->get_style_context()->add_provider_for_screen(this->pWindow->get_screen(), cssProvider,
                                                                GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    this->gladeBuilder->get_widget("profileList", this->profileList);
    this->gladeBuilder->get_widget("screenSwitcher", this->screenSwitcher);

    this->setupNewProfileScreen();
    this->setupEditProfileScreen();
    this->setupAboutScreen();

    this->drawProfileList();
}

GUI::~GUI() {
    delete this->pWindow;
}

void GUI::saveProfiles() {
    this->logger->debug(this->translator->trns("Generating final XML for saving..."));
    auto resolvedOptions = this->resolver->resolveOptionsForSave(
            this->systemWideConfiguration, this->driverConfiguration, this->userDefinedConfiguration,
            this->availableGPUs
    );
    auto rawXML = this->writer->generateRawXml(resolvedOptions);
    this->logger->debug(
            Glib::ustring::compose(this->translator->trns("Writing generated XML: %1"), rawXML)
    );
    std::string userHome(std::getenv("HOME"));
    std::ofstream outFile(userHome + "/.drirc");
    outFile << rawXML;
    outFile.close();
}

Gtk::Window *GUI::getWindowPointer() {
    return this->pWindow;
}

void GUI::setupLocale() {
    boost::locale::generator gen;
    std::locale l = gen("");
    std::locale::global(l);
    std::cout.imbue(l);
    textdomain("adriconf");

    Glib::ustring langCode(std::use_facet<boost::locale::info>(l).language());

    this->logger->debug(
            Glib::ustring::compose(this->translator->trns("Current language code is %1"), langCode)
    );

    this->locale = langCode;
}

void GUI::drawProfileList() {
    this->profileList->forall([this](Gtk::Widget &widget) {
        this->profileList->remove(widget);
    });

    for (auto &driver : this->userDefinedConfiguration) {
        for (auto &possibleApp : driver->getApplications()) {
            auto itemBox = Gtk::manage(new Gtk::Button);
            itemBox->set_label(possibleApp->getName());
            itemBox->set_visible(true);
            itemBox->get_style_context()->add_class("profileItem");
            itemBox->signal_clicked().connect(sigc::bind<Profile_ptr, Device_ptr>(
                    sigc::mem_fun(this, &GUI::onApplicationSelected), possibleApp, driver
            ));

            this->profileList->add(*itemBox);
        }
    }

    auto newButton = Gtk::manage(new Gtk::Button(Gtk::Stock::ADD));
    newButton->set_visible(true);
    newButton->get_style_context()->add_class("profileItem");
    newButton->signal_clicked().connect([this]() { this->screenSwitcher->set_visible_child("newProfileScreen"); });
    this->profileList->add(*newButton);

    auto aboutButton = Gtk::manage(new Gtk::Button(Gtk::Stock::ABOUT));
    aboutButton->set_visible(true);
    aboutButton->get_style_context()->add_class("profileItem");
    aboutButton->signal_clicked().connect([this]() { this->screenSwitcher->set_visible_child("aboutScreen"); });
    this->profileList->add(*aboutButton);
}


void GUI::drawApplicationOptions() {
    auto selectedAppOptions = this->currentApp->getOptions();

    /* Get the notebook itself */
    Gtk::Notebook *pNotebook;
    this->gladeBuilder->get_widget("editProfileContent", pNotebook);
    if (!pNotebook) {
        this->logger->error(this->translator->trns("Notebook object not found in glade file!"));
        return;
    }

    /* Remove any previous defined page */
    int numberOfPages = pNotebook->get_n_pages();

    for (int i = 0; i < numberOfPages; i++) {
        pNotebook->remove_page(-1);
    }

    /* Remove any previous defined spinButton */
    this->currentSpinButtons.clear();

    pNotebook->set_visible(true);

    std::list<Section> definedSections;
    if (this->currentApp->getIsUsingPrime()) {
        definedSections = this->availableGPUs[this->currentApp->getDevicePCIId()]->getSections();
    } else {
        auto driverConfigurationIter = std::find_if(
                this->driverConfiguration.begin(), this->driverConfiguration.end(),
                [this](const DriverConfiguration &configuration) {
                    return this->currentDriver->getDriver() == configuration.getDriverName();
                });

        if (driverConfigurationIter == this->driverConfiguration.end()) {
            this->logger->error(
                    Glib::ustring::compose(this->translator->trns(
                            "User-defined configuration with driver %1 doesn't has valid configuration loaded."),
                                           this->currentDriver->getDriver())
            );
            return;
        }

        definedSections = driverConfigurationIter->getSections();
    }

    /* Draw each section as a tab */
    for (auto &section : definedSections) {

        Gtk::Box *tabBox = Gtk::manage(new Gtk::Box);
        tabBox->set_visible(true);
        tabBox->set_orientation(Gtk::Orientation::ORIENTATION_VERTICAL);
        tabBox->set_margin_start(8);
        tabBox->set_margin_end(8);
        tabBox->set_margin_top(10);


        /* Draw each field individually */
        for (auto &option : section.getOptions()) {
            auto optionValue = std::find_if(selectedAppOptions.begin(), selectedAppOptions.end(),
                                            [&option](ProfileOption_ptr o) {
                                                return option.getName() == o->getName();
                                            });

            ProfileOption_ptr currentOption;

            if (optionValue == selectedAppOptions.end()) {
                currentOption = std::make_shared<ProfileOption>();
                currentOption->setName(option.getName());
                currentOption->setValue(option.getDefaultValue());
                this->currentApp->addOption(currentOption);
            } else {
                currentOption = *optionValue;
            }

            Gtk::Box *optionBox = Gtk::manage(new Gtk::Box);
            optionBox->set_visible(true);
            optionBox->set_orientation(Gtk::Orientation::ORIENTATION_HORIZONTAL);
            optionBox->set_margin_bottom(10);

            if (option.getType() == DriverOptionType::BOOL || option.getType() == DriverOptionType::FAKE_BOOL) {
                OptionSwitch *optionSwitch = Gtk::manage(new OptionSwitch(currentOption));

                optionBox->pack_end(*optionSwitch, false, false);
            }

            if (option.getType() == DriverOptionType::ENUM) {
                auto optionCombo = Gtk::manage(new OptionCombo(currentOption));

                int counter = 0;
                for (auto const &enumOption : option.getEnumValues()) {
                    optionCombo->append(enumOption.first, enumOption.second);

                    if (enumOption.second == currentOption->getValue()) {
                        optionCombo->set_active(counter);
                    }
                    counter++;
                }

                optionBox->pack_end(*optionCombo, false, false);
            }

            if (option.getType() == DriverOptionType::INT) {
                auto optionEntry = Gtk::manage(new OptionSpin(currentOption, option.getValidValueStart(), option.getValidValueEnd()));

                optionBox->pack_end(*optionEntry, false, true);
            }

            Gtk::Label *label = Gtk::manage(new Gtk::Label);
            label->set_label(option.getDescription());
            label->set_visible(true);
            label->set_justify(Gtk::Justification::JUSTIFY_LEFT);
            label->set_line_wrap(true);
            label->set_margin_start(10);
            optionBox->pack_start(*label, false, true);

            tabBox->add(*optionBox);
        }


        Gtk::ScrolledWindow *scrolledWindow = Gtk::manage(new Gtk::ScrolledWindow);
        scrolledWindow->set_visible(true);
        scrolledWindow->add(*tabBox);

        pNotebook->append_page(*scrolledWindow, section.getDescription());
    }

    /* If we have more than one GPU then we are under PRIME */
    if (this->availableGPUs.size() > 1) {
        auto primeOptionPtr = std::find_if(selectedAppOptions.begin(), selectedAppOptions.end(),
                                           [](const ProfileOption_ptr &a) {
                                               return a->getName() == "device_id";
                                           });

        ProfileOption_ptr primeOption;
        if (primeOptionPtr == selectedAppOptions.end()) {
            primeOption = std::make_shared<ProfileOption>();
            primeOption->setName("device_id");
            primeOption->setValue("");
            this->currentApp->addOption(primeOption);
        } else {
            primeOption = *primeOptionPtr;
        }

        this->shouldIgnorePrimeComboBoxEvents = true;
        if (this->primeComboBox == nullptr) {
            this->primeComboBox = std::make_shared<ComboBoxExtra<std::pair<Glib::ustring, ProfileOption_ptr>>>();
            this->primeComboBox->signal_changed().connect(sigc::mem_fun(this, &GUI::onPrimeComboBoxChanged));
        } else {
            this->primeComboBox->removeAllChildren();
        }

        this->primeComboBox->set_visible(true);

        this->primeComboBox->append(this->translator->trns("Use default GPU of screen"),
                                    std::make_pair("", primeOption));

        this->primeComboBox->set_active(0);

        int counter = 0;
        for (auto const &enumOption : this->availableGPUs) {
            counter++;
            this->primeComboBox->append(enumOption.second->getDeviceName(),
                                        std::make_pair(enumOption.second->getPciId(), primeOption));

            if (this->currentApp->getDevicePCIId() == enumOption.second->getPciId()) {
                this->primeComboBox->set_active(counter);
            }
        }

        this->shouldIgnorePrimeComboBoxEvents = false;

        auto *label = Gtk::manage(new Gtk::Label);
        label->set_label(this->translator->trns("Force Application to use GPU"));
        label->set_visible(true);
        label->set_justify(Gtk::Justification::JUSTIFY_LEFT);
        label->set_line_wrap(true);
        label->set_margin_start(10);

        auto *optionBox = Gtk::manage(new Gtk::Box);
        optionBox->set_visible(true);
        optionBox->set_orientation(Gtk::Orientation::ORIENTATION_HORIZONTAL);
        optionBox->set_margin_bottom(10);
        optionBox->pack_end(*(this->primeComboBox), false, false);
        optionBox->pack_start(*label, false, true);

        auto primeTabBox = Gtk::manage(new Gtk::Box);
        primeTabBox->set_visible(true);
        primeTabBox->set_orientation(Gtk::Orientation::ORIENTATION_VERTICAL);
        primeTabBox->set_margin_start(8);
        primeTabBox->set_margin_end(8);
        primeTabBox->set_margin_top(10);
        primeTabBox->add(*optionBox);

        Gtk::ScrolledWindow *scrolledWindow = Gtk::manage(new Gtk::ScrolledWindow);
        scrolledWindow->set_visible(true);
        scrolledWindow->add(*primeTabBox);

        pNotebook->append_page(*scrolledWindow, this->translator->trns("PRIME Settings"));
    }

}

void GUI::setupAboutScreen() {
    Gtk::Label *versionLabel, *gitRevisionLabel;

    this->gladeBuilder->get_widget("versionLabel", versionLabel);
    this->gladeBuilder->get_widget("gitRevisionLabel", gitRevisionLabel);
    versionLabel->set_label(Glib::ustring::compose(
            this->translator->trns("version: %1"), BUILD_VERSION_NUMBER
    ));
    gitRevisionLabel->set_label(Glib::ustring::compose(
            this->translator->trns("git-revision: %1"), GIT_COMMIT_HASH
    ));

    Gtk::Button* aboutScreenBackButton;
    this->gladeBuilder->get_widget("aboutScreenBackButton", aboutScreenBackButton);
    aboutScreenBackButton->signal_clicked().connect(sigc::mem_fun(this, &GUI::onBackToProfileListPressed));
}

void GUI::onBackToProfileListPressed() {
    this->screenSwitcher->set_visible_child("listProfilesScreen");
}

void GUI::setupNewProfileScreen() {
    this->gladeBuilder->get_widget("newProfileBackButton", this->newProfileBackButton);
    this->gladeBuilder->get_widget("newProfileSaveButton", this->newProfileSaveButton);

    this->newProfileBackButton->signal_clicked().connect(sigc::mem_fun(this, &GUI::onBackToProfileListPressed));
    this->newProfileSaveButton->signal_clicked().connect(sigc::mem_fun(this, &GUI::onNewProfileSaveClicked));

    // Setup the driver comboBox
    this->gladeBuilder->get_widget_derived("newAppDriver", this->newProfileDriver);
    for (const auto &driverConfig : this->driverConfiguration) {
        this->newProfileDriver->append(driverConfig.getDriverName(), driverConfig);
    }

    this->newProfileDriver->set_active(0);

    // Setup text boxes
    this->newProfileSaveButton->set_sensitive(false);
    this->gladeBuilder->get_widget("newAppName", this->newProfileName);
    this->gladeBuilder->get_widget("newAppExecutable", this->newProfileExecutable);

    this->newProfileName->signal_changed().connect(sigc::mem_fun(this, &GUI::onNewApplicationTextFieldChanged));
    this->newProfileExecutable->signal_changed().connect(
            sigc::mem_fun(this, &GUI::onNewApplicationTextFieldChanged)
    );
}

void GUI::onNewApplicationTextFieldChanged() {
    this->newProfileSaveButton->set_sensitive(
            this->newProfileName->get_text_length() > 0 && this->newProfileExecutable->get_text_length() > 0
    );
}

void GUI::onNewProfileSaveClicked() {
    auto driver = this->newProfileDriver->getActiveExtraData();

    Profile_ptr newProfile = driver.generateApplication();
    newProfile->setExecutable(this->newProfileExecutable->get_text());
    newProfile->setName(this->newProfileName->get_text());

    this->resolver->setSystemWideDefaults(newProfile, this->systemWideConfiguration, driver.getDriverName());

    for (auto &userConfig : this->userDefinedConfiguration) {
        if (userConfig->getDriver() == driver.getDriverName()) {
            userConfig->addApplication(newProfile);
            userConfig->sortApplications();
        }
    }

    this->saveProfiles();
    this->drawProfileList();
    this->onBackToProfileListPressed();
    this->newProfileName->set_text("");
    this->newProfileExecutable->set_text("");
}

void GUI::onApplicationSelected(const Profile_ptr &selectedProfile, Device_ptr selectedProfileDriver) {
    this->currentApp = std::make_shared<ShadowProfile>(selectedProfile);;
    this->currentDriver = selectedProfileDriver;

    this->drawApplicationOptions();
    this->screenSwitcher->set_visible_child("editProfileScreen");
}

void GUI::setupEditProfileScreen() {
    this->gladeBuilder->get_widget("editProfileBackButton", this->editProfileBackButton);
    this->gladeBuilder->get_widget("editProfileSaveButton", this->editProfileSaveButton);
    this->gladeBuilder->get_widget("editProfileRemoveButton", this->editProfileRemoveButton);

    this->editProfileBackButton->signal_clicked().connect(sigc::mem_fun(this, &GUI::onBackToProfileListPressed));
    this->editProfileSaveButton->signal_clicked().connect(sigc::mem_fun(this, &GUI::onEditProfileSaveClicked));
    this->editProfileRemoveButton->signal_clicked().connect(sigc::mem_fun(this, &GUI::onEditProfileRemoveClicked));
}

void GUI::onEditProfileSaveClicked() {
    this->currentApp->applyChangesToOriginalData();
    this->saveProfiles();
    this->onBackToProfileListPressed();
}

void GUI::onEditProfileRemoveClicked() {
    Gtk::MessageDialog confirmDialog(*(this->pWindow), "Are you sure?", false, Gtk::MESSAGE_QUESTION,
                                     Gtk::BUTTONS_OK_CANCEL, true);
    confirmDialog.set_name("confirmProfileRemoveDialog");
    int result = confirmDialog.run();

    if (result == Gtk::RESPONSE_OK) {
        this->currentDriver->getApplications().remove_if([this](const Profile_ptr &app) {
            return app->getExecutable() == this->currentApp->getExecutable();
        });

        this->saveProfiles();
        this->drawProfileList();
        this->onBackToProfileListPressed();
    }
}

void GUI::onPrimeComboBoxChanged() {
    if (this->shouldIgnorePrimeComboBoxEvents) {
        return;
    }

    auto extraData = this->primeComboBox->getActiveExtraData();
    auto selectedGpuPciId = extraData.first;
    auto profileOption = extraData.second;

    profileOption->setValue(selectedGpuPciId);

    /* Reset to default */
    if (selectedGpuPciId.empty()) {
        this->currentApp->setIsUsingPrime(false);
        this->currentApp->setDevicePCIId("");
        this->currentApp->setPrimeDriverName("");
    } else {
        /* Set this app as a prime-enabled app */
        this->currentApp->setPrimeDriverName(
                this->availableGPUs[selectedGpuPciId]->getDriverName()
        );
        this->currentApp->setIsUsingPrime(true);
        this->currentApp->setDevicePCIId(selectedGpuPciId);

        /* Add the missing options of the new driver */
        auto newDriverOptions = this->availableGPUs[selectedGpuPciId]->getOptionsMap();
        this->resolver->addMissingDriverOptions(this->currentApp, newDriverOptions);
    }

    this->drawApplicationOptions();
}
